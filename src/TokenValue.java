/**
 * File: TokenValue
 * Author: Michelle John
 * Date: 14 Oct 2018
 * Purpose: Project 2: Calculator
 */
import java.util.Objects;

/**
 * Class to hold the token type and the value of the token.
 */
public class TokenValue {

  private GrammarTokens token;
  private String value;

  /**
   * Constructor.
   *
   * @param token the token
   * @param value the value of the token to be used (not necessarily the value of the enum)
   */
  TokenValue(GrammarTokens token, String value) {
    this.token = token;
    this.value = value;
  }

  /**
   * @return the token
   */
  GrammarTokens getToken() {
    return token;
  }

  /**
   * @param token the token to set
   */
  public void setToken(GrammarTokens token) {
    this.token = token;
  }

  /**
   * @return the value of the token (not necessarily the enum value)
   */
  String getValue() {
    return value;
  }

  /**
   * @param value the value of the token
   */
  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public int hashCode() {
    return Objects.hash(token, value);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof TokenValue)) {
      return false;
    }
    TokenValue other = (TokenValue) obj;
    return Objects.equals(token, other.token)
        && Objects.equals(value, other.value);
  }

  @Override
  public String toString() {
    return "\nToken: " + token.toString() + "\nValue: " + value;
  }
}
