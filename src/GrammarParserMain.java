/**
 * File: GrammarParserMain
 * Author: Michelle John
 * Date: 14 Oct 2018
 * Purpose: Project 2: Calculator
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class that is the entry into the GUI parser application.
 */
public class GrammarParserMain {

  /**
   * Main method from which the application starts.
   *
   * @param args the arguments for the application
   */
  public static void main(String[] args) {
    GrammarParserMain app = new GrammarParserMain();
    app.start();
  }

  /**
   * Starts the application.
   */
  private void start() {
    String inputFile = readInputFile("input.txt");
    if (!inputFile.startsWith(GrammarTokens.WINDOW.getValue())) {
      System.err.println("Invalid grammar input.");
      System.exit(1);
    }

    new GuiBuilder(tokenizeInput(inputFile));
  }


  /**
   * Reads the input file and stores it as a {@link String} for further processing.
   *
   * @param fileName the name of the data file
   * @return the contents of the file as a string
   */
  private String readInputFile(String fileName) {
    String lineRead;
    StringBuilder fileString = new StringBuilder();
    if (Files.exists(Paths.get(fileName))) {
      try (FileReader fileReader = new FileReader(fileName);
           BufferedReader reader = new BufferedReader(fileReader)) {
        lineRead = reader.readLine();
        while (lineRead != null) {
          fileString.append(lineRead);
          lineRead = reader.readLine();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return fileString.toString();
  }

  /**
   * Takes an input {@link String} and turns it into a {@link List} of {@link TokenValue}s.
   *
   * @param tokenString the string to tokenize
   * @return the list of tokens
   */
  private List<TokenValue> tokenizeInput(String tokenString) {
    List<TokenValue> tokenList = new ArrayList<>();
    String remainingString = tokenString;
    char nextChar = remainingString.charAt(0);
    int index;

    while (!remainingString.isEmpty()) {
      switch (nextChar) {
        case ' ':
          remainingString = remainingString.substring(1);
          break;
        case 'W':
          index = remainingString.indexOf('w');
          tokenList.add(new TokenValue(GrammarTokens.WINDOW, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case '\"':
          remainingString = remainingString.substring(1);
          index = remainingString.indexOf('\"');
          tokenList.add(new TokenValue(GrammarTokens.STRING, remainingString.substring(0, index)));
          remainingString = remainingString.substring(index + 1);
          break;
        case '(':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.LEFT_PAREN, "("));
          break;
        case ')':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.RIGHT_PAREN, ")"));
          break;
        case 'L':
          if (remainingString.charAt(2) == 'y') {
            index = remainingString.indexOf('t');
            tokenList.add(new TokenValue(GrammarTokens.LAYOUT, remainingString.substring(0, index + 1)));
          } else {
            index = remainingString.indexOf('l');
            tokenList.add(new TokenValue(GrammarTokens.LABEL, remainingString.substring(0, index + 1)));
          }
          remainingString = remainingString.substring(index + 1);
          break;
        case 'F':
          index = remainingString.indexOf('w');
          tokenList.add(new TokenValue(GrammarTokens.FLOW, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case 'E':
          index = remainingString.indexOf('d');
          tokenList.add(new TokenValue(GrammarTokens.END, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case 'G':
          if (remainingString.charAt(2) == 'i') {
            index = remainingString.indexOf('d');
            tokenList.add(new TokenValue(GrammarTokens.GRID, remainingString.substring(0, index + 1)));
          } else {
            index = remainingString.indexOf('p');
            tokenList.add(new TokenValue(GrammarTokens.GROUP, remainingString.substring(0, index + 1)));
          }
          remainingString = remainingString.substring(index + 1);
          break;
        case 'B':
          index = remainingString.indexOf('n');
          tokenList.add(new TokenValue(GrammarTokens.BUTTON, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case 'P':
          index = remainingString.indexOf('l');
          tokenList.add(new TokenValue(GrammarTokens.PANEL, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case 'T':
          index = remainingString.indexOf('d');
          tokenList.add(new TokenValue(GrammarTokens.TEXTFIELD, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case 'R':
          index = remainingString.indexOf('o');
          tokenList.add(new TokenValue(GrammarTokens.RADIO, remainingString.substring(0, index + 1)));
          remainingString = remainingString.substring(index + 1);
          break;
        case ',':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.COMMA, ","));
          break;
        case '.':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.PERIOD, "."));
          break;
        case ':':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.COLON, ":"));
          break;
        case ';':
          remainingString = remainingString.substring(1);
          tokenList.add(new TokenValue(GrammarTokens.SEMI_COLON, ";"));
          break;
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
          StringBuilder number = new StringBuilder();
          while(isCharInt(nextChar) != -1) {
            number.append(nextChar);
            remainingString = remainingString.substring(1);
            nextChar = remainingString.isEmpty() ? Character.MIN_VALUE : remainingString.charAt(0);
          }
          tokenList.add(new TokenValue(GrammarTokens.NUMBER, number.toString()));
          break;
        default:
          System.out.println("The application encountered an undefined token. Grammar will be parsed with errors");
          remainingString = "";
          break;
      }
      nextChar = remainingString.isEmpty() ? Character.MIN_VALUE : remainingString.charAt(0);
    }
    return tokenList;
  }


  /**
   * Determines if a character is an integer.
   *
   * @param characterToCheck the character to check
   * @return the integer the character parses to, or -1 if the character doesn't parse to an integer
   */
  private int isCharInt(char characterToCheck) {
    try {
      return Integer.parseInt(Character.toString(characterToCheck));
    } catch (NumberFormatException ex) {
      return -1;
    }
  }
}
