/**
 * File: GrammarTokens
 * Author: Michelle John
 * Date: 14 Oct 2018
 * Purpose: Project 2: Calculator
 */

/**
 * Enum to define the token types of the grammar.
 */
public enum GrammarTokens {
  WINDOW("Window"),
  STRING("String"),
  NUMBER("Number"),
  END("End"),
  LAYOUT("Layout"),
  FLOW("Flow"),
  GRID("Grid"),
  BUTTON("Button"),
  GROUP("Group"),
  LABEL("Label"),
  PANEL("Panel"),
  TEXTFIELD("Textfield"),
  RADIO("Radio"),
  LEFT_PAREN("("),
  RIGHT_PAREN(")"),
  COMMA(","),
  PERIOD("."),
  COLON(":"),
  SEMI_COLON(";");

  private String value;

  /**
   * Constructor.
   *
   * @param value the value of the token
   */
  GrammarTokens(String value) {
    this.value = value;
  }

  /**
   * @return the value of the enum
   */
  public String getValue() {
    return this.value;
  }
}
