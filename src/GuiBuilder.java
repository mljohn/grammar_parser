/**
 * File: GuiBuilder
 * Author: Michelle John/Thomas Ott
 * Date: 14 Oct 2018
 * Purpose: Project 2: Calculator
 */
import javax.swing.*;
import java.awt.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * Class that takes the {@link List} of {@link TokenValue}s and creates the defined GUI.
 */
class GuiBuilder {

  private List<TokenValue> tokenList;
  private JTextField text;

  /**
   * Constructor.
   *
   * @param tokenList the {@link List} of {@link TokenValue}s from which to build the GUI
   */
  GuiBuilder(List<TokenValue> tokenList) {
    this.tokenList = tokenList;
    JFrame frame = buildGui();
    frame.setVisible(true);
  }

  /**
   * Method that builds the individual GUI elements.
   *
   * @return the assembled {@link JFrame}
   */
  private JFrame buildGui() {
    int index = 0;
    Deque<Container> parents = new ArrayDeque<>();
    ButtonGroup curGroup = null;
    List<String> mathButtonText = new ArrayList<>();

    try {
      while (index < tokenList.size()) {
        switch (tokenList.get(index).getToken()) {
          case WINDOW:
            JFrame frame = createJFrame(tokenList, index);
            parents.push(frame);
            index += 7;
            break;
          case LAYOUT:
            index++;
            // on "Grid" or "Flow" now
            LayoutManager layout;
            if (tokenList.get(index).getToken().equals(GrammarTokens.GRID)) { // layout conditionals
              int param1 = parseToInt(tokenList.get(index + 2).getValue());
              int param2 = parseToInt(tokenList.get(index + 4).getValue());
              if (tokenList.get(index + 5).getValue().equals(")")) {
                // 2 number case
                layout = new GridLayout(param1, param2);
                index += 6;
              } else {
                // 4 number case
                int param3 = parseToInt(tokenList.get(index + 6).getValue());
                int param4 = parseToInt(tokenList.get(index + 8).getValue());
                layout = new GridLayout(param1, param2, param3, param4);
                index += 10;
              }
            } else {
              layout = new FlowLayout();
              index++;
            }
            parents.peek().setLayout(layout);
            break;
          case COLON:
            index++;
            break;
          case PANEL:
            JPanel panel = new JPanel();
            parents.peek().add(panel);
            parents.push(panel);
            index++;
            break;
          case BUTTON:
            String buttonText = tokenList.get(index + 1).getValue();
            JButton button = new JButton(buttonText);
            button.addActionListener(e -> {
              switch (buttonText) {
                case "C":
                  text.setText("");
                  mathButtonText.clear();
                  break;
                case "-": case "+": case "/": case "*":
                  // this case statement will fall through to the next if it doesn't go through the else
                  if (mathButtonText.isEmpty()) {
                    mathButtonText.add(buttonText);
                  } else {
                    showMessageDialog(null, "There are too many operators. Please just use one.");
                    break;
                  }
                case "0": case "1": case "2": case "3": case "4": case "5": case "6": case "7": case "8": case "9":
                  text.setText(text.getText() + buttonText);
                  break;
                case "=":
                  if (mathButtonText.isEmpty()) {
                    showMessageDialog(null, "There is no operator. Please re-enter the expression.");
                    break;
                  }
                  String textString = text.getText();
                  if (textString == null || textString.isEmpty()) {
                    showMessageDialog(null, "There is nothing in the text box. No math will be done.");
                    break;
                  }
                  String operator = mathButtonText.get(0);
                  // + and * are metacharacters and need to be escaped for the split to work properly
                  if ("+".equals(operator) || "*".equals(operator)) {
                    operator = operator.replace(operator, "\\" + operator);
                  }
                  List<String> expression = stream(textString.split(operator)).collect(Collectors.toList());

                  int firstInteger = 0;
                  int secondInteger = 0;

                  if (expression.size() !=2 ) {
                    showMessageDialog(null, "There are not the right number of operands. Please enter 2 operands.");
                    break;
                  }
                  try {
                    firstInteger = Integer.parseInt(expression.get(0));
                    secondInteger = Integer.parseInt(expression.get(1));
                  } catch (NumberFormatException ex) {
                    showMessageDialog(null, "Invalid numbers were entered. Please only enter integers.");
                  }
                  if ("/".equals(mathButtonText.get(0)) && (0 == secondInteger)) {
                    showMessageDialog(null, "Cannot divide by zero.");
                    break;
                  }
                  switch (mathButtonText.get(0)) {
                    case "-":
                      showMessageDialog(null, firstInteger - secondInteger);
                      break;
                    case "+":
                      showMessageDialog(null, firstInteger + secondInteger);
                      break;
                    case "/":
                      // There has already been a divide by zero check, so no need to do it here
                      showMessageDialog(null, firstInteger / (double) secondInteger);
                      break;
                    case "*":
                      showMessageDialog(null, firstInteger * secondInteger);
                      break;
                  }
                  break;
                default:
                  // This case should never be reached
                  showMessageDialog(null, "Invalid button.");
                  break;
              }
            });
            parents.peek().add(button);
            index += 3;
            break;
          case GROUP:
            curGroup = new ButtonGroup();
            index++;
            break;
          case RADIO:
            JRadioButton radio = new JRadioButton(tokenList.get(index + 1).getValue());
            parents.peek().add(radio);
            curGroup.add(radio);
            index += 3;
            break;
          case TEXTFIELD:
            text = new JTextField(parseToInt(tokenList.get(index + 1).getValue()));
            parents.peek().add(text);
            index += 3;
            break;
          case LABEL:
            JLabel label = new JLabel(tokenList.get(index + 1).getValue());
            parents.peek().add(label);
            index += 3;
            break;
          case END:
            index++;
            if (tokenList.get(index).getToken().equals(GrammarTokens.PERIOD)) {
              return (JFrame) parents.peek();
            }
            parents.pop();
            index++;
            break;
          default:
            System.out.println(index + " " + tokenList.get(index));
            return null;
        }
      }
    } catch (Exception e) {
      System.err.println("UNEXPECTED TOKEN " + tokenList.get(index).toString() + " NEAR INDEX " + index);
      return null;
    }
    return null;
  }

  /**
   * Method to create the {@link JFrame} as defined in the input file.
   *
   * @param tokenList the {@link List} of {@link TokenValue}s
   * @param index the element from which to start building the {@link JFrame}
   * @return the {@link JFrame} that was created
   * @throws Exception if an element doesn't parse to an integer
   */
  private JFrame createJFrame(List<TokenValue> tokenList, int index) throws Exception {
    JFrame frame = new JFrame(tokenList.get(index + 1).getValue());
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setSize(parseToInt(tokenList.get(index + 3).getValue()), parseToInt(tokenList.get(index + 5).getValue()));
    return frame;
  }

  /**
   * Method to parse a number represented as a {@link String} to an {@code int}.
   *
   * @param num the {@link String} to parse
   * @return the parsed {@code int}
   * @throws Exception if the {@link String} can't be parsed to an {@code int}
   */
  private int parseToInt(String num) throws Exception {
    try {
      return Integer.parseInt(num);
    } catch(Exception e) {
      // ¯\_(ツ)_/¯
      throw new Exception("¯\\_(ツ)_/¯", e);
    }
  }
}
